Installation instructions for OLIMEX USB-SERIAL-CABLE  

1.Disconnect the USB-SERIAL-CABLE cable from your computer.  
2.Install the PL2303_Prolific_GPS_AllInOne_1013.exe  
3.Connect USB-SERIAL-CABLE  
4.You should now have a new COM port in device manager  

If you have installed previous drivers you should uninstall them.  
If you had installed the driver while USB-SERIAL-CABLE was plugged you will need to restart your computer.  
